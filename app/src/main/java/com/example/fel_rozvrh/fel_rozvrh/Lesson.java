package com.example.fel_rozvrh.fel_rozvrh;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Lesson {

    @Id
    private long id;
    private long idSub;

    private int hour;
    private int min;
    private int dayNum;
    private int len;

    private String name;

    private String type;
    private String room;
    private String teacher;
    private String day;
    private String length;
    private String time;


    public Lesson(String type,
                  String room,
                  String teacher,
                  String day,
                  String time,
                  String length){
        this.type = type;
        this.room = room;
        this.teacher = teacher;
        saveDay(day);
        saveTime(time);
        saveLen(length);
    }

    public Lesson( long id,
                   long idSub,
                   int hour,
                   int min,
                   int dayNum,
                   int len,
                   String name,
                   String type,
                   String room,
                   String teacher,
                   String day,
                   String length,
                   String time) {
        this.id = id;
        this.idSub = idSub;
        this.hour = hour;
        this.min = min;
        this.dayNum = dayNum;
        this.len = len;
        this.name = name;
        this.type = type;
        this.room = room;
        this.teacher = teacher;
        this.day = day;
        this.length = length;
        this.time = time;
    }

    private void saveLen(String l){
        len = 0;
        length = l;
        boolean ok = true;
        for (int i = 0; i<l.length(); ++i) {
            int n = l.charAt(i) - 48;

            if(n<10)
                len = len*10 + n;
            else
                ok = false;
        }
        if (!ok){
            length = "error";
            len = 0;
        }
    }

    private void saveTime(String time) {
        min = hour = 0;
        boolean ok = true;
        int i = 0;
        if (time.length() > 2 && time.length() < 6) {
            hour = time.charAt(i++) - 48;
            if (time.charAt(i) != ':') hour = (hour * 10) + time.charAt(i++) - 48;
            if (time.charAt(i++) != ':') {
                i = 0;
                ok = false;
            }
            min = time.charAt(i++) - 48;
            if (time.length() == i + 1) min = (min * 10) + time.charAt(i) - 48;
            if (min > 59 || hour > 23) ok = false;
        }
        if (!ok) {
            min = hour = 0;
            this.time = "error";
        } else {
            this.time = time;
        }
    }

    private void saveDay(String d){
        String[] days = {"Po","Ut","St","Ct","Pa","So","Ne"};
        boolean ok = true;

        if(d.length()==1){
            dayNum = d.charAt(0)-48;
            if(0 < dayNum && dayNum < 8)
                day = days[dayNum-1];
            else
                ok = false;
        } else
            ok = false;
        if (!ok){
            day = "error";
            dayNum = 0;
        }
    }


    public long getId() {
        return id;
    }

    public long getIdSub() {
        return idSub;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public int getDayNum() {
        return dayNum;
    }

    public int getLen() {
        return len;
    }

    public String getType() {
        return type;
    }

    public String getRoom() {
        return room;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getDay() {
        return day;
    }

    public String getLength() {
        return length;
    }

    public String getTime() {
        return time;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setIdSub(long idSub) {
        this.idSub = idSub;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setDayNum(int dayNum) {
        this.dayNum = dayNum;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setDay(String day) {
        saveDay(day);
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
