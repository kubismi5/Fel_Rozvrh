package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedList;

public class MyLinkListAdapter extends RecyclerView.Adapter<MyLinkListAdapter.MyLinkViewHolder> {
    private final LinkedList<MyLink> mMyLinkList;
    private LayoutInflater mInflater;

    public MyLinkListAdapter(Context context, LinkedList<MyLink> wordList) {
        mInflater = LayoutInflater.from(context);
        this.mMyLinkList = wordList;
    }

    @Override
    public MyLinkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.link_list_item, parent, false);
        return new MyLinkViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(MyLinkViewHolder holder, int position) {
        MyLink mCurrent = mMyLinkList.get(position);
        holder.linkItemView.setText(mCurrent.getName());
        holder.linkItemView2.setText(mCurrent.getText());
    }

    @Override
    public int getItemCount() {
        return mMyLinkList.size();
    }

    class MyLinkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final TextView linkItemView;
        public final TextView linkItemView2;
        final MyLinkListAdapter mAdapter;

        public MyLinkViewHolder(View itemView, MyLinkListAdapter adapter) {
            super(itemView);
            linkItemView = (TextView) itemView.findViewById(R.id.link_name_label);
            linkItemView2 = (TextView) itemView.findViewById(R.id.link_text_label);
            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int mPosition = getLayoutPosition();
            long element = mMyLinkList.get(mPosition).getId();
            Context context = view.getContext();
            Intent intent = new Intent(context, EditLinkActivity.class);
            intent.putExtra("a", element);
            context.startActivity(intent);
        }
    }



}
