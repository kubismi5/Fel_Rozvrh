package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextView username;
    private Button changeLogin;
    private CheckBox labs;
    private CheckBox lectures;
    private CheckBox camera;
    private Spinner color;
    private Spinner orientation;
    private Button confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        changeLogin = findViewById(R.id.chageLoginOnSettingsID);
        changeLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        color = findViewById(R.id.chooseColorSpinnerID);
        List<String> categories = new ArrayList<>();
        categories.add("Blue");
        categories.add("Pink");
        categories.add("Yellow");
        categories.add("Choose color of your theme");
        final int listsize = categories.size() - 1;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories){
            @Override
            public int getCount() {
                return(listsize);
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        color.setAdapter(dataAdapter);
        color.setSelection(listsize);
        color.setOnItemSelectedListener(this);

        orientation = findViewById(R.id.chooseOrientationSpinnerID);
        List<String> orientationList = new ArrayList<>();
        orientationList.add("Vertical");
        orientationList.add("Horizontal");
        orientationList.add("Choose orientation of your timetable");
        final int orientationListSize = orientationList.size() - 1;

        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, orientationList){
            @Override
            public int getCount() {
                return(orientationListSize);
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orientation.setAdapter(dataAdapter);
        orientation.setSelection(orientationListSize);
        orientation.setOnItemSelectedListener(this);

        confirm = findViewById(R.id.confirmSettingsButtonID);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, FirstScreenActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//        Toast.makeText(adapterView.getContext(),
//                "OnItemSelectedListener : " + adapterView.getItemAtPosition(i).toString(),
//                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
