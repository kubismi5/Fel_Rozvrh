package com.example.fel_rozvrh.fel_rozvrh;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class EditLessonActivity extends AppCompatActivity implements View.OnClickListener{


    Context context;

    private Lesson lesson;
    private LinkedList<Lesson> mLessonList = new LinkedList<>();
    private Box<Lesson> lessonBox;
    private Query<Lesson> lessonQuery;
    private EditText lType;
    private EditText lTeacher;
    private EditText lRoom;
    private EditText lTime;
    private EditText lDay;
    private EditText lLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_lesson);

        Intent i = getIntent();
        long id = i.getLongExtra("a", 1);

        mLessonList = new LinkedList<>();

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        lessonBox = boxStore.boxFor(Lesson.class);

        lessonQuery = lessonBox.query().build();
        List<Lesson> lessonQueryResult = lessonQuery.find();
        mLessonList.clear();
        mLessonList.addAll(lessonQueryResult);

        for(Lesson l : mLessonList){
            if(l.getId() == id){
                lesson= l;
                break;
            }
        }

        lType = (EditText) findViewById(R.id.dialog_edit_lesson_type);
        lType.setText(lesson.getType());
        lTeacher = (EditText) findViewById(R.id.dialog_edit_lesson_teacher);
        lTeacher.setText(lesson.getTeacher());
        lRoom = (EditText) findViewById(R.id.dialog_edit_lesson_room);
        lRoom.setText(lesson.getRoom());
        lTime = (EditText) findViewById(R.id.dialog_edit_lesson_time);
        lTime.setText(lesson.getTime());
        lDay = (EditText) findViewById(R.id.dialog_edit_lesson_day);
        lDay.setText(lesson.getDayNum());
        lLength = (EditText) findViewById(R.id.dialog_edit_lesson_length);
        lLength.setText(lesson.getLength());

        Button buttonSave = (Button) findViewById(R.id.btnSaveEditLesson);
        Button buttonRemove = (Button) findViewById(R.id.btnRemoveEditLesson);

        buttonSave.setOnClickListener(this);
        buttonRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnSaveEditLesson){
            if(lType.getText().toString().equals(""))
                lesson.setType("error");
            else lesson.setType(lType.getText().toString());

            if(lTeacher.getText().toString().equals(""))
                lesson.setTeacher("error");
            else lesson.setTeacher(lTeacher.getText().toString());

            if(lRoom.getText().toString().equals(""))
                lesson.setRoom("error");
            else lesson.setRoom(lRoom.getText().toString());

            if(lDay.getText().toString().equals(""))
                lesson.setDay("error");
            else lesson.setDay(lDay.getText().toString());

            if(lLength.getText().toString().equals(""))
                lesson.setLength("error");
            else lesson.setLength(lLength.getText().toString());

            if(lTime.getText().toString().equals(""))
                lesson.setTime("error");
            else lesson.setTime(lTime.getText().toString());

            lessonBox.put(lesson);
            finish();
        } else if(v.getId() == R.id.btnRemoveEditLesson) {
            List<Lesson> lessonQueryResult = lessonQuery.find();
            mLessonList.clear();
            mLessonList.addAll(lessonQueryResult);
            LinkedList<Lesson> newList = new LinkedList<>();
            for (Lesson l : mLessonList) {
                if (l.getId() != lesson.getId()) {
                    newList.add(l);
                }
            }

            lessonQuery.remove();
            mLessonList.clear();
            mLessonList.addAll(lessonQuery.find());

            for (Lesson l : newList) {
                lessonBox.put(l);
            }
            finish();
        }

    }



}

