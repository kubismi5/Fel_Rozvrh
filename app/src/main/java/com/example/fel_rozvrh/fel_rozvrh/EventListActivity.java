package com.example.fel_rozvrh.fel_rozvrh;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class EventListActivity extends AppCompatActivity implements AddEventDialog.NoticeDialogListener
{
    private LinkedList<Event> mEveList = new LinkedList<>();
    private Box<Event> eveBox;
    private Query<Event> eveQuery;

    private RecyclerView mRecyclerView;
    private EventListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);


        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        eveBox = boxStore.boxFor(Event.class);

        eveQuery = eveBox.query().build();
        List<Event> eveQueryResult = eveQuery.find();

        mEveList.clear();
        mEveList.addAll(eveQueryResult);

        mRecyclerView = (RecyclerView) findViewById(R.id.event_list_recycler_view);
        mAdapter = new EventListAdapter(this, mEveList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.event_list_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment addPoiDialogFragment = new AddEventDialog();
                addPoiDialogFragment.show(getSupportFragmentManager(), "newEvent");
            }
        });
    }
    @Override
    public void onResume(){
        super.onResume();
        createRVEve(null,false);
    }

    private void createRVEve(Event event, boolean push){
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        eveBox = boxStore.boxFor(Event.class);
        eveQuery = eveBox.query().build();

        if(push) eveBox.put(event);

        List<Event> eveQueryResult = eveQuery.find();

        mEveList.clear();
        mEveList.addAll(eveQueryResult);

        mRecyclerView = (RecyclerView) findViewById(R.id.event_list_recycler_view);
        mAdapter = new EventListAdapter(this, mEveList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onDialogPositiveClick(Event event) {
        createRVEve(event, true);

    }

    @Override
    public void onDialogNegativeClick() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_event_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete_event_list_menu_item:
                eveQuery.remove();
                mEveList.clear();
                mEveList.addAll(eveQuery.find());
                mRecyclerView.getAdapter().notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
