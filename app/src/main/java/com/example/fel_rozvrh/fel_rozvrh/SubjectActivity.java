package com.example.fel_rozvrh.fel_rozvrh;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;


public class SubjectActivity extends AppCompatActivity implements View.OnClickListener
{

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Button btnSelect;
    private ImageView ivImage;
    private String userChoosenTask;
    private boolean isImageFitToScreen;
    private Animator mCurrentAnimator;


    private LinkedList<MyLink> mMyLinkList = new LinkedList<>();
    private Box<MyLink> myLinkBox;
    private Query<MyLink> myLinkQuery;
    private MyLinkListAdapter mAdapterLink;
    private RecyclerView mRecyclerViewLink;

    private LinkedList<Lesson> mLessonList = new LinkedList<>();
    private Box<Lesson> lessonBox;
    private Query<Lesson> lessonQuery;
    private LessonListAdapter mAdapterLesson;
    private RecyclerView mRecyclerViewLesson;

//    private LinkedList<Photo> mPhotoList = new LinkedList<>();
//    private Box<Photo> photoBox;
//    private Query<Photo> photoQuery;


    private GridLayout mlinklayout;
    private GridLayout mlessonlayout;
    Button additem;
    DynamicViews dnv;
    Context context;

    private TextView mTextViewName;
    private TextView mTextViewCode;
    //Subject02 sub;

    private Subject sub;
    private LinkedList<Subject> mSubList = new LinkedList<>();
    private Box<Subject> subBox;
    private Query<Subject> subQuery;
    private long id;

    LinkedList<TextView> linkTextView = new LinkedList<>();
    private int lastSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        Intent i = getIntent();
        id = i.getLongExtra("a", 1);

        mSubList = new LinkedList<>();

        setSubTextView();

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        subBox = boxStore.boxFor(Subject.class);

        subQuery = subBox.query().build();
        List<Subject> subQueryResult = subQuery.find();

        mSubList.clear();
        mSubList.addAll(subQueryResult);

        for(Subject s : mSubList){
            if(s.getId() == id){
                sub = s;
                break;
            }
        }

        btnSelect = (Button) findViewById(R.id.buttonEditPhoto);
        btnSelect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        ivImage = (ImageView) findViewById(R.id.imageView3);

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

//        String check = AppPreferences.getInstance(getApplicationContext()).getPath();
//        if(check!=null&&!check.equals("-1")){
//            ivImage.setImageURI(Uri.parse(check));
//        }

        mlinklayout = (GridLayout) findViewById(R.id.linklayout);
        mlessonlayout = (GridLayout) findViewById(R.id.lessonlayout);

        mTextViewName = (TextView)findViewById(R.id.textName);
        mTextViewCode = (TextView)findViewById(R.id.textCode);

        mTextViewName.setText(sub.getName());
        mTextViewCode.setText(sub.getCode());

        createRVLink(true);
        createRVLesson(false);


        Button linkButton= (Button)findViewById(R.id.buttonEditLinks);
        Button lessonButton = (Button)findViewById(R.id.buttonEditLessons);
        Button nameButton = (Button)findViewById(R.id.buttonEditName);

        linkButton.setOnClickListener(this);
        lessonButton.setOnClickListener(this);
        nameButton.setOnClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        int REQUEST_PERMISSION= 100;
        int cameraPermission = this.checkSelfPermission(Manifest.permission.CAMERA);
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (cameraPermission != PackageManager.PERMISSION_GRANTED  ) {
                this.requestPermissions(
                        new String[]{Manifest.permission.CAMERA },
                        REQUEST_PERMISSION
                );
            }
        }
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(SubjectActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(SubjectActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result) {
                        cameraIntent();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
                //System.out.println(data.getData().getPath());
                //AppPreferences.getInstance(getApplicationContext()).setPath(data.getData().getPath());
            }
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
                //System.out.println(data.getData().getPath());
                //AppPreferences.getInstance(getApplicationContext()).setPath(data.getData().getPath());
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory()+ "/Pictures",
                System.currentTimeMillis() + ".jpg");
        System.out.println(destination.toString());

//        Toast.makeText(this, destination.toString(), Toast.LENGTH_LONG).show();

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ivImage.setImageBitmap(bm);
    }

    public static String getFileToByte(String path){
        Bitmap bm = null;
        ByteArrayOutputStream baos = null;
        byte[] b = null;
        String encodeString = null;
        try{
            bm = BitmapFactory.decodeFile(path);
            baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            b = baos.toByteArray();
            encodeString = Base64.encodeToString(b, Base64.DEFAULT);
        }catch (Exception e){
            e.printStackTrace();
        }
        return encodeString;
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonEditLinks){
            Context context = v.getContext();
            Intent intent = new Intent(context, MyLinkListActivity.class);
            intent.putExtra("idSub", id);
            context.startActivity(intent);
        } else if(v.getId() == R.id.buttonEditLessons){
            Context context = v.getContext();
            Intent intent = new Intent(context, LessonListActivity.class);
            intent.putExtra("idSub", id);
            context.startActivity(intent);
        }else if(v.getId() == R.id.buttonEditName){
            saveName(v);
        }

    }
    @Override
    public void onResume(){
        super.onResume();
        createRVLink(false);
        createRVLesson(false);
    }

    private void restart(){
        finish();
        startActivity(getIntent());
    }
    public void saveName(View view){

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(SubjectActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.edit_name_code, null);
        final EditText eName= (EditText) mView .findViewById(R.id.dialog_add_subject_name);
        final EditText eCode= (EditText) mView .findViewById(R.id.dialog_add_subject_code);
        Button mLogin = (Button) mView .findViewById(R.id.btnSave);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sub.setName(eName.getText().toString());
                sub.setCode(eCode.getText().toString());
                subBox.put(sub);
                createRVLesson(true);
                mTextViewName.setText(sub.getName());
                mTextViewCode.setText(sub.getCode());

                dialog.dismiss();
            }
        });

    }


    private void createRVLink(boolean firstime) {
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        myLinkBox = boxStore.boxFor(MyLink.class);

        myLinkQuery = myLinkBox.query().build();
        List<MyLink> myLinkQueryResult = myLinkQuery.find();
        mMyLinkList.clear();
        mMyLinkList.addAll(myLinkQueryResult);

        LinkedList<MyLink> thisLinks = new LinkedList<>();

        for (MyLink l : mMyLinkList)
            if(l.getIdSub() == id)
                thisLinks.add(l);

        if(firstime)lastSize = thisLinks.size();
        else {
            if(lastSize!= thisLinks.size()){
                restart();
                return;
            }
        }

        mlinklayout.clearAnimation();
        mlinklayout = (GridLayout) findViewById(R.id.linklayout);

        for (int i = 0; i < mlinklayout.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mlinklayout.getChildAt(i);
            if(i<thisLinks.size()) {
                final int finalI = i;
                //linkTextView.get(i).setText(mMyLinkList.get(i).getName());
                cardView.setMinimumHeight(50);

                Spanned Text = Html.fromHtml("<a href='"+thisLinks.get(i).getText()+"/'>"+ thisLinks.get(i).getName() +"</a>");
                linkTextView.get(i).setMovementMethod(LinkMovementMethod.getInstance());
                linkTextView.get(i).setText(Text);


            } else {
                cardView.removeAllViews();
            }
        }
    }

    private void createRVLesson(boolean newName){
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        lessonBox = boxStore.boxFor(Lesson.class);

        lessonQuery = lessonBox.query().build();
        List<Lesson> lessonQueryResult = lessonQuery.find();

        LinkedList<Lesson> newList = new LinkedList<>();
        newList.clear();
        mLessonList.clear();
        newList.addAll(lessonQueryResult);
        for(Lesson l : newList){
            if(l.getIdSub() == id){
                mLessonList.add(l);
                if(newName){
                    l.setName(sub.getName());
                    lessonBox.put(l);
                }
            }
        }

        mlessonlayout.removeAllViews();
        dnv = new DynamicViews(context);
        int i = 0;
        for(Lesson l : mLessonList) {
            mlessonlayout.addView(dnv.descriptionTextView(getApplicationContext(), l.getDay()), i++);
            mlessonlayout.addView(dnv.descriptionTextView(getApplicationContext(), l.getType()), i++);
            mlessonlayout.addView(dnv.descriptionTextView(getApplicationContext(), l.getRoom()), i++);
            mlessonlayout.addView(dnv.descriptionTextView(getApplicationContext(), l.getTime()), i++);
            mlessonlayout.addView(dnv.descriptionTextView(getApplicationContext(), l.getLength()), i++);
            mlessonlayout.addView(dnv.descriptionTextView(getApplicationContext(), l.getTeacher()), i++);
        }
    }

    private void setSubTextView(){
        linkTextView.add((TextView)findViewById(R.id.link1));
        linkTextView.add((TextView)findViewById(R.id.link2));
        linkTextView.add((TextView)findViewById(R.id.link3));
        linkTextView.add((TextView)findViewById(R.id.link4));
        linkTextView.add((TextView)findViewById(R.id.link5));
        linkTextView.add((TextView)findViewById(R.id.link6));
        linkTextView.add((TextView)findViewById(R.id.link7));
        linkTextView.add((TextView)findViewById(R.id.link8));
        linkTextView.add((TextView)findViewById(R.id.link9));
        linkTextView.add((TextView)findViewById(R.id.link10));
    }


}

