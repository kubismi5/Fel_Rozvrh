package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.Intent;

import java.util.LinkedList;

public class SubjectListAdapter extends RecyclerView.Adapter<SubjectListAdapter.PoiViewHolder> {
    private final LinkedList<Subject> mSubjectList;
    private LayoutInflater mInflater;

    SubjectListAdapter(Context context, LinkedList<Subject> wordList) {
        mInflater = LayoutInflater.from(context);
        this.mSubjectList = wordList;
    }

    @NonNull
    @Override
    public PoiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.subject_list_item, parent, false);
        return new PoiViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull PoiViewHolder holder, int position) {
        Subject mCurrent = mSubjectList.get(position);
        holder.subItemViewName.setText(mCurrent.getName());
        holder.subItemViewCode.setText(mCurrent.getCode());

    }

    @Override
    public int getItemCount() {
        return mSubjectList.size();
    }

    class PoiViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final TextView subItemViewName;
        final TextView subItemViewCode;

        final SubjectListAdapter mAdapter;

        PoiViewHolder(View itemView, SubjectListAdapter adapter) {
            super(itemView);
            subItemViewName = (TextView) itemView.findViewById(R.id.sub_name_label);
            subItemViewCode= (TextView) itemView.findViewById(R.id.sub_code_label);

            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int mPosition = getLayoutPosition();
            long element = mSubjectList.get(mPosition).getId();
            Context context = view.getContext();
            Intent intent = new Intent(context, EditSubjectActivity.class);
            intent.putExtra("a", element);
            context.startActivity(intent);
        }
    }
}
