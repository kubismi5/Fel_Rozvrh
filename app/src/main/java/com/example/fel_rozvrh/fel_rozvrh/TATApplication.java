package com.example.fel_rozvrh.fel_rozvrh;
import android.app.Application;

import io.objectbox.BoxStore;

public class TATApplication extends Application {
    private BoxStore boxStore;

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        // do this once, for example in your Application class
        boxStore = MyObjectBox.builder().androidContext(TATApplication.this).build();
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }

}