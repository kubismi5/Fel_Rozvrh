package com.example.fel_rozvrh.fel_rozvrh;

import android.annotation.SuppressLint;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class SubjectListActivity extends AppCompatActivity implements AddSubjectDialog.NoticeDialogListener
{
    private LinkedList<Subject> mSubList = new LinkedList<>();
    private Box<Subject> subBox;
    private Query<Subject> subQuery;

    private RecyclerView mRecyclerView;
    private SubjectListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_list);


        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        subBox = boxStore.boxFor(Subject.class);

        subQuery = subBox.query().build();
        List<Subject> subQueryResult = subQuery.find();

        mSubList.clear();
        mSubList.addAll(subQueryResult);

        mRecyclerView = (RecyclerView) findViewById(R.id.sub_list_recycler_view);
        mAdapter = new SubjectListAdapter(this, mSubList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.sub_list_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment addPoiDialogFragment = new AddSubjectDialog();
                addPoiDialogFragment.show(getSupportFragmentManager(), "newSubject");
            }
        });
    }
    @Override
    public void onResume(){
        super.onResume();
        createRVSub(null,false);
    }

    private void createRVSub(Subject sub, boolean push){
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        subBox = boxStore.boxFor(Subject.class);
        subQuery = subBox.query().build();

        if(push) subBox.put(sub);

        List<Subject> subQueryResult = subQuery.find();

        mSubList.clear();
        mSubList.addAll(subQueryResult);

        mRecyclerView = (RecyclerView) findViewById(R.id.sub_list_recycler_view);
        mAdapter = new SubjectListAdapter(this, mSubList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onDialogPositiveClick(Subject sub) {
        createRVSub(sub, true);

    }

    @Override
    public void onDialogNegativeClick() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_subject_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete_subject_list_menu_item:
                subQuery.remove();
                mSubList.clear();
                mSubList.addAll(subQuery.find());
                mRecyclerView.getAdapter().notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
