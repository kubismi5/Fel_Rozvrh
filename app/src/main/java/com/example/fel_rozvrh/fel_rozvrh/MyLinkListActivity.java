package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class MyLinkListActivity extends AppCompatActivity implements AddMyLinkDialog.NoticeDialogListener
{
    private LinkedList<MyLink> mMyLinkList = new LinkedList<>();
    private Box<MyLink> myLinkBox;
    private Query<MyLink> myLinkQuery;

    private RecyclerView mRecyclerView;
    private MyLinkListAdapter mAdapter;
    private long idSub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_list);

        Intent i = getIntent();
        idSub = i.getLongExtra("idSub", 1);

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        myLinkBox = boxStore.boxFor(MyLink.class);

        // query all pois, sorted a-z by their code
        myLinkQuery = myLinkBox.query().build();
        List<MyLink> myLinkQueryResult = myLinkQuery.find();
        LinkedList<MyLink> newList = new LinkedList<>();
        newList.clear();
        mMyLinkList.clear();
        newList.addAll(myLinkQueryResult);
        for(MyLink l : newList){
            if(l.getIdSub() == idSub){
                mMyLinkList.add(l);
            }
        }
        // Get a handle to the RecyclerView.
        mRecyclerView = (RecyclerView) findViewById(R.id.link_list_recycler_view);
        // Create an adapter and supply the data to be displayed.
        mAdapter = new MyLinkListAdapter(this, mMyLinkList);
        // Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
        // Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Add a floating action click handler for creating new entries.
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.link_list_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment addPoiDialogFragment = new AddMyLinkDialog();
                addPoiDialogFragment.show(getSupportFragmentManager(), "newlink");
            }
        });
    }
    @Override
    public void onResume(){
        super.onResume();
        createRVLink(null,false);
    }

    private void createRVLink(MyLink myLink, boolean push){
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        myLinkBox = boxStore.boxFor(MyLink.class);
        myLinkQuery = myLinkBox.query().build();

        if(push) {
            myLink.setIdSub(idSub);
            myLinkBox.put(myLink);
        }

        List<MyLink> myLinkQueryResult = myLinkQuery.find();
        LinkedList<MyLink> newList = new LinkedList<>();
        newList.clear();
        mMyLinkList.clear();
        newList.addAll(myLinkQueryResult);
        for(MyLink l : newList){
            if(l.getIdSub() == idSub){
                mMyLinkList.add(l);
            }
        }
        // Get a handle to the RecyclerView.
        mRecyclerView = (RecyclerView) findViewById(R.id.link_list_recycler_view);
        // Create an adapter and supply the data to be displayed.
        mAdapter = new MyLinkListAdapter(this, mMyLinkList);
        // Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
        // Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onDialogPositiveClick(MyLink myLink) {
        createRVLink(myLink, true);
    }

    @Override
    public void onDialogNegativeClick() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_link_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete_link_list_menu_item:

                List<MyLink> myLinkQueryResult = myLinkQuery.find();
                mMyLinkList.clear();
                mMyLinkList.addAll(myLinkQueryResult);
                LinkedList<MyLink> newList = new LinkedList<>();
                for(MyLink l : mMyLinkList){
                    if(l.getIdSub() != idSub){
                        newList.add(l);
                    }
                }

                myLinkQuery.remove();
                mMyLinkList.clear();
                mMyLinkList.addAll(myLinkQuery.find());
                mRecyclerView.getAdapter().notifyDataSetChanged();

                for(MyLink l : newList){
                    myLinkBox.put(l);
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
