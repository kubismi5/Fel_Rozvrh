package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedList;

public class LessonListAdapter extends RecyclerView.Adapter<LessonListAdapter.LessonViewHolder> {
    private final LinkedList<Lesson> mLessonList;
    private LayoutInflater mInflater;

    public LessonListAdapter(Context context, LinkedList<Lesson> wordList) {
        mInflater = LayoutInflater.from(context);
        this.mLessonList = wordList;
    }

    @Override
    public LessonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.lesson_list_item, parent, false);
        return new LessonViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(LessonViewHolder holder, int position) {
        Lesson mCurrent = mLessonList.get(position);
        holder.lessonItemViewType.setText(mCurrent.getType());
        holder.lessonItemViewRoom.setText(mCurrent.getRoom());
        holder.lessonItemViewTeacher.setText(mCurrent.getTeacher());
        holder.lessonItemViewTime.setText(mCurrent.getTime());
        holder.lessonItemViewDay.setText(mCurrent.getDay());
        holder.lessonItemViewLength.setText(mCurrent.getLength());
    }

    @Override
    public int getItemCount() {
        return mLessonList.size();
    }

    class LessonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final TextView lessonItemViewType;
        public final TextView lessonItemViewRoom;
        public final TextView lessonItemViewTeacher;
        public final TextView lessonItemViewTime;
        public final TextView lessonItemViewDay;
        public final TextView lessonItemViewLength;
        final LessonListAdapter mAdapter;

        public LessonViewHolder(View itemView, LessonListAdapter adapter) {
            super(itemView);
            lessonItemViewType = (TextView) itemView.findViewById(R.id.lesson_type_label);
            lessonItemViewTeacher = (TextView) itemView.findViewById(R.id.lesson_teacher_label);
            lessonItemViewRoom = (TextView) itemView.findViewById(R.id.lesson_room_label);
            lessonItemViewTime = (TextView) itemView.findViewById(R.id.lesson_time_label);
            lessonItemViewDay = (TextView) itemView.findViewById(R.id.lesson_day_label);
            lessonItemViewLength = (TextView) itemView.findViewById(R.id.lesson_length_label);

            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int mPosition = getLayoutPosition();
            long element = mLessonList.get(mPosition).getId();
            Context context = view.getContext();
            Intent intent = new Intent(context, EditLessonActivity.class);
            intent.putExtra("a", element);
            context.startActivity(intent);
        }
    }



}
