package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class LessonListActivity extends AppCompatActivity implements AddLessonDialog.NoticeDialogListener
{
    private LinkedList<Lesson> mLessonList = new LinkedList<>();
    private Box<Lesson> lessonBox;
    private Query<Lesson> lessonQuery;

    private RecyclerView mRecyclerView;
    private LessonListAdapter mAdapter;

    private long idSub;
    private String subName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_list);

        Intent i = getIntent();
        idSub = i.getLongExtra("idSub", 1);

        setSub();

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        lessonBox = boxStore.boxFor(Lesson.class);

        // query all pois, sorted a-z by their code
        lessonQuery = lessonBox.query().build();
        List<Lesson> poiQueryResult = lessonQuery.find();

        LinkedList<Lesson> newList = new LinkedList<>();
        newList.clear();
        mLessonList.clear();
        newList.addAll(poiQueryResult);
        for(Lesson l : newList){
            if(l.getIdSub() == idSub){
                mLessonList.add(l);
            }
        }

        // Get a handle to the RecyclerView.
        mRecyclerView = (RecyclerView) findViewById(R.id.lesson_list_recycler_view);
        // Create an adapter and supply the data to be displayed.
        mAdapter = new LessonListAdapter(this, mLessonList);
        // Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
        // Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Add a floating action click handler for creating new entries.
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.lesson_list_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment addLessonDialog= new AddLessonDialog();
                addLessonDialog.show(getSupportFragmentManager(), "newLesson");
            }
        });
    }

    private void setSub(){
        LinkedList<Subject> subList = new LinkedList<>();
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        Box<Subject> subBox = boxStore.boxFor(Subject.class);

        Query<Subject> subQuery = subBox.query().build();
        List<Subject> subQueryResult = subQuery.find();
        subList.clear();
        subList.addAll(subQueryResult);

        for (Subject s : subList){
            if(s.getId() == idSub){
                subName = s.getName();
                break;
            }
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        createRVLesson(null,false);
    }

    private void createRVLesson(Lesson lesson, boolean push){
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        lessonBox = boxStore.boxFor(Lesson.class);
        lessonQuery = lessonBox.query().build();


        if(push) {
            lesson.setIdSub(idSub);
            lesson.setName(subName);
            lessonBox.put(lesson);
        }

        List<Lesson> lessonQueryResult = lessonQuery.find();
        LinkedList<Lesson> newList = new LinkedList<>();
        newList.clear();
        mLessonList.clear();
        newList.addAll(lessonQueryResult);
        for(Lesson l : newList){
            if(l.getIdSub() == idSub){
                mLessonList.add(l);
            }
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.lesson_list_recycler_view);
        // Create an adapter and supply the data to be displayed.
        mAdapter = new LessonListAdapter(this, mLessonList);
        // Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
        // Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    @Override
    public void onDialogPositiveClick(Lesson lesson) {
        createRVLesson(lesson,true);

    }

    @Override
    public void onDialogNegativeClick() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_lesson_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete_lesson_list_menu_item:

                List<Lesson> lessonQueryResult = lessonQuery.find();
                mLessonList.clear();
                mLessonList.addAll(lessonQueryResult);
                LinkedList<Lesson> newList = new LinkedList<>();
                for(Lesson l : mLessonList){
                    if(l.getIdSub() != idSub){
                        newList.add(l);
                    }
                }

                lessonQuery.remove();
                mLessonList.clear();
                mLessonList.addAll(lessonQuery.find());
                mRecyclerView.getAdapter().notifyDataSetChanged();

                for(Lesson l : newList){
                    lessonBox.put(l);
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
