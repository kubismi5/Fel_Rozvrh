package com.example.fel_rozvrh.fel_rozvrh;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class MyLink {

    @Id
    private long id;
    private long idSub;

    private String name;
    private String text;

    public MyLink(long id, long idSub,String name, String text){
        this.id = id;
        this.idSub = idSub;
        this.name = name;
        this.text = text;
    }

    public MyLink(String name, String text){
        this.name = name;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getIdSub() {
        return idSub;
    }

    public void setIdSub(long idSub) {
        this.idSub = idSub;
    }
}
