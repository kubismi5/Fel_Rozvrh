package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class EditLinkActivity extends AppCompatActivity implements View.OnClickListener{


    Context context;

    private MyLink myLink;
    private LinkedList<MyLink> mMyLinkList = new LinkedList<>();
    private Box<MyLink> myLinkBox;
    private Query<MyLink> myLinkQuery;
    private EditText eCode;
    private EditText eName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_link);

        Intent i = getIntent();
        long id = i.getLongExtra("a", 1);

        mMyLinkList = new LinkedList<>();

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        myLinkBox = boxStore.boxFor(MyLink.class);

        myLinkQuery = myLinkBox.query().build();
        List<MyLink> myLinkQueryResult = myLinkQuery.find();
        mMyLinkList.clear();
        mMyLinkList.addAll(myLinkQueryResult);

        for(MyLink l : mMyLinkList){
            if(l.getId() == id){
                myLink = l;
                break;
            }
        }


        //View mView = getLayoutInflater().inflate(R.layout.edit_link, null);
        eCode= (EditText) findViewById(R.id.dialog_add_edit_link_code);
        eName= (EditText) findViewById(R.id.dialog_add_edit_link_name);
        eCode.setText(myLink.getText());
        eName.setText(myLink.getName());
        Button buttonSave = (Button) findViewById(R.id.btnSaveEditLink);
        Button buttonRemove = (Button) findViewById(R.id.btnRemoveEditLink);

        buttonSave.setOnClickListener(this);
        buttonRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnSaveEditLink){
            if(eName.getText().toString().equals(""))
                myLink.setName("error");
            else
                myLink.setName(eName.getText().toString());
            if(eCode.getText().toString().equals(""))
                myLink.setText("error");
            else
                myLink.setText(eCode.getText().toString());
            myLinkBox.put(myLink);
            finish();
        } else if(v.getId() == R.id.btnRemoveEditLink) {
            List<MyLink> myLinkQueryResult = myLinkQuery.find();
            mMyLinkList.clear();
            mMyLinkList.addAll(myLinkQueryResult);
            LinkedList<MyLink> newList = new LinkedList<>();
            for (MyLink l : mMyLinkList) {
                if (l.getId() != myLink.getId()) {
                    newList.add(l);
                }
            }

            myLinkQuery.remove();
            mMyLinkList.clear();
            mMyLinkList.addAll(myLinkQuery.find());

            for (MyLink l : newList) {
                myLinkBox.put(l);
            }
            finish();
        }

    }



}
