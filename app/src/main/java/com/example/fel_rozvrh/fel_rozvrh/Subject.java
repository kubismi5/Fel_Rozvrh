package com.example.fel_rozvrh.fel_rozvrh;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Subject {

    @Id
    private  long id;

    private long myId;
    private String name;
    private String code;

    public Subject(long id, long myId,String name, String code){
        this.id = id;
        this.myId = myId;
        this.name = name;
        this.code = code;
    }

    public Subject(long myId,String name, String code){
        this.myId = myId;
        this.name = name;
        this.code = code;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getMyId() {
        return myId;
    }

    public void setMyId(long myId) {
        this.myId = myId;
    }
}
