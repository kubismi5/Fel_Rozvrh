package com.example.fel_rozvrh.fel_rozvrh;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class TimetableActivity extends Activity {


    LinkedList<TextView> tv = new LinkedList<>();
    int height, width;

    LinearLayout ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.time_table);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        saveTimetable();
    }

    private void saveTimetable(){
        LinkedList<LinkedList<Lesson>> llist = getListLesson();

        setTextView01();
        setTextView02(llist.get(0));
        setTextView03(llist.get(1));
        setTextView04(llist.get(2));
        setTextView05(llist.get(3));
        setTextView06(llist.get(4));
        setTextView07(llist.get(5));
        setTextView08(llist.get(6));

    }


    private LinkedList<LinkedList<Lesson>> getListLesson(){
        LinkedList<Lesson> mLessonList = new LinkedList<>();
        LinkedList<LinkedList<Lesson>> ret = new LinkedList<>();
        LinkedList<Lesson> l02 = new LinkedList<>();
        LinkedList<Lesson> l03 = new LinkedList<>();
        LinkedList<Lesson> l04 = new LinkedList<>();
        LinkedList<Lesson> l05 = new LinkedList<>();
        LinkedList<Lesson> l06 = new LinkedList<>();
        LinkedList<Lesson> l07 = new LinkedList<>();
        LinkedList<Lesson> l08 = new LinkedList<>();

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        Box<Lesson> lessonBox = boxStore.boxFor(Lesson.class);
        Query<Lesson> lessonQuery = lessonBox.query().build();
        List<Lesson> lessonQueryResult = lessonQuery.find();
        mLessonList.clear();
        mLessonList.addAll(lessonQueryResult);
        for (Lesson l : mLessonList){
            if(6< l.getHour() && l.getHour() < 9) l02.add(l);
            if(9<= l.getHour() && l.getHour() < 11) l03.add(l);
            if(11<= l.getHour() && l.getHour() < 13) l04.add(l);
            if(12< l.getHour() && l.getHour() < 15) l05.add(l);
            if(14< l.getHour() && l.getHour() < 16) l06.add(l);
            if(16<= l.getHour() && l.getHour() < 18) l07.add(l);
            if(18<= l.getHour() && l.getHour() < 20) l08.add(l);
        }
        ret.add(l02);
        ret.add(l03);
        ret.add(l04);
        ret.add(l05);
        ret.add(l06);
        ret.add(l07);
        ret.add(l08);

        return ret;
    }

    @SuppressLint("SetTextI18n")
    private void setTextView01() {
        ll = (LinearLayout) findViewById(R.id.ll01);
        ll.setMinimumWidth(100);
        tv.add((TextView)findViewById(R.id.tv0101));
        tv.add((TextView)findViewById(R.id.tv0102));
        tv.add((TextView)findViewById(R.id.tv0103));
        tv.add((TextView)findViewById(R.id.tv0104));
        tv.add((TextView)findViewById(R.id.tv0105));
        tv.add((TextView)findViewById(R.id.tv0106));

        tv.get(1).setText(" Mo");
        tv.get(2).setText(" Tu");
        tv.get(3).setText(" We");
        tv.get(4).setText(" Th");
        tv.get(5).setText(" Fr");
        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth(100);
        }

    }

    @SuppressLint("SetTextI18n")
    private void setTextView02(LinkedList<Lesson> llist) {
        ll = (LinearLayout) findViewById(R.id.ll02);
        ll.setMinimumWidth((width-100)/7);

        tv.clear();
        tv.add((TextView)findViewById(R.id.tv0201));
        tv.add((TextView)findViewById(R.id.tv0202));
        tv.add((TextView)findViewById(R.id.tv0203));
        tv.add((TextView)findViewById(R.id.tv0204));
        tv.add((TextView)findViewById(R.id.tv0205));
        tv.add((TextView)findViewById(R.id.tv0206));

        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth((width-100)/7);
        }
        tv.get(0).setText("7:30-9:00");
        for(Lesson l : llist) {
            String s = l.getType() + " " + l.getName() + " " + l.getRoom();
            if(l.getDayNum() == 1) tv.get(1).setText(s);
            else if(l.getDayNum() == 2) tv.get(2).setText(s);
            else if(l.getDayNum() == 3) tv.get(3).setText(s);
            else if(l.getDayNum() == 4) tv.get(4).setText(s);
            else if(l.getDayNum() == 5) tv.get(5).setText(s);
        }
    }
    @SuppressLint("SetTextI18n")
    private void setTextView03(LinkedList<Lesson> llist) {
        ll = (LinearLayout) findViewById(R.id.ll03);
        ll.setMinimumWidth((width-100)/7);

        tv.clear();
        tv.add((TextView)findViewById(R.id.tv0301));
        tv.add((TextView)findViewById(R.id.tv0302));
        tv.add((TextView)findViewById(R.id.tv0303));
        tv.add((TextView)findViewById(R.id.tv0304));
        tv.add((TextView)findViewById(R.id.tv0305));
        tv.add((TextView)findViewById(R.id.tv0306));

        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth((width-100)/7);
        }
        tv.get(0).setText("9:15-10:45");
        for(Lesson l : llist) {
            String s = l.getType() + " " + l.getName() + " " + l.getRoom();
            if(l.getDayNum() == 1) tv.get(1).setText(s);
            else if(l.getDayNum() == 2) tv.get(2).setText(s);
            else if(l.getDayNum() == 3) tv.get(3).setText(s);
            else if(l.getDayNum() == 4) tv.get(4).setText(s);
            else if(l.getDayNum() == 5) tv.get(5).setText(s);
        }
    }
    @SuppressLint("SetTextI18n")
    private void setTextView04(LinkedList<Lesson> llist) {
        ll = (LinearLayout) findViewById(R.id.ll04);
        ll.setMinimumWidth((width-100)/7);

        tv.clear();
        tv.add((TextView)findViewById(R.id.tv0401));
        tv.add((TextView)findViewById(R.id.tv0402));
        tv.add((TextView)findViewById(R.id.tv0403));
        tv.add((TextView)findViewById(R.id.tv0404));
        tv.add((TextView)findViewById(R.id.tv0405));
        tv.add((TextView)findViewById(R.id.tv0406));

        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth((width-100)/7);
        }
        tv.get(0).setText("11:00-12:30");
        for(Lesson l : llist) {
            String s = l.getType() + " " + l.getName() + " " + l.getRoom();
            if(l.getDayNum() == 1) tv.get(1).setText(s);
            else if(l.getDayNum() == 2) tv.get(2).setText(s);
            else if(l.getDayNum() == 3) tv.get(3).setText(s);
            else if(l.getDayNum() == 4) tv.get(4).setText(s);
            else if(l.getDayNum() == 5) tv.get(5).setText(s);
        }

    }
    @SuppressLint("SetTextI18n")
    private void setTextView05(LinkedList<Lesson> llist) {
        ll = (LinearLayout) findViewById(R.id.ll05);
        ll.setMinimumWidth((width-100)/7);

        tv.clear();
        tv.add((TextView)findViewById(R.id.tv0501));
        tv.add((TextView)findViewById(R.id.tv0502));
        tv.add((TextView)findViewById(R.id.tv0503));
        tv.add((TextView)findViewById(R.id.tv0504));
        tv.add((TextView)findViewById(R.id.tv0505));
        tv.add((TextView)findViewById(R.id.tv0506));

        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth((width-100)/7);
        }
        tv.get(0).setText("12:45-14:15");
        for(Lesson l : llist) {
            String s = l.getType() + " " + l.getName() + " " + l.getRoom();
            if(l.getDayNum() == 1) tv.get(1).setText(s);
            else if(l.getDayNum() == 2) tv.get(2).setText(s);
            else if(l.getDayNum() == 3) tv.get(3).setText(s);
            else if(l.getDayNum() == 4) tv.get(4).setText(s);
            else if(l.getDayNum() == 5) tv.get(5).setText(s);
        }

    }
    @SuppressLint("SetTextI18n")
    private void setTextView06(LinkedList<Lesson> llist) {
        ll = (LinearLayout) findViewById(R.id.ll06);
        ll.setMinimumWidth((width-100)/7);

        tv.clear();
        tv.add((TextView)findViewById(R.id.tv0601));
        tv.add((TextView)findViewById(R.id.tv0602));
        tv.add((TextView)findViewById(R.id.tv0603));
        tv.add((TextView)findViewById(R.id.tv0604));
        tv.add((TextView)findViewById(R.id.tv0605));
        tv.add((TextView)findViewById(R.id.tv0606));

        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth((width-100)/7);
        }
        tv.get(0).setText("14:30-16:00");
        for(Lesson l : llist) {
            String s = l.getType() + " " + l.getName() + " " + l.getRoom();
            if(l.getDayNum() == 1) tv.get(1).setText(s);
            else if(l.getDayNum() == 2) tv.get(2).setText(s);
            else if(l.getDayNum() == 3) tv.get(3).setText(s);
            else if(l.getDayNum() == 4) tv.get(4).setText(s);
            else if(l.getDayNum() == 5) tv.get(5).setText(s);
        }

    }
    @SuppressLint("SetTextI18n")
    private void setTextView07(LinkedList<Lesson> llist) {
        ll = (LinearLayout) findViewById(R.id.ll07);
        ll.setMinimumWidth((width-100)/7);

        tv.clear();
        tv.add((TextView)findViewById(R.id.tv0701));
        tv.add((TextView)findViewById(R.id.tv0702));
        tv.add((TextView)findViewById(R.id.tv0703));
        tv.add((TextView)findViewById(R.id.tv0704));
        tv.add((TextView)findViewById(R.id.tv0705));
        tv.add((TextView)findViewById(R.id.tv0706));

        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth((width-100)/7);
        }
        tv.get(0).setText("16:15-17:45");
        for(Lesson l : llist) {
            String s = l.getType() + " " + l.getName() + " " + l.getRoom();
            if(l.getDayNum() == 1) tv.get(1).setText(s);
            else if(l.getDayNum() == 2) tv.get(2).setText(s);
            else if(l.getDayNum() == 3) tv.get(3).setText(s);
            else if(l.getDayNum() == 4) tv.get(4).setText(s);
            else if(l.getDayNum() == 5) tv.get(5).setText(s);
        }

    }

    @SuppressLint("SetTextI18n")
    private void setTextView08(LinkedList<Lesson> llist) {
        ll = (LinearLayout) findViewById(R.id.ll08);
        ll.setMinimumWidth((width-100)/7);

        tv.clear();
        tv.add((TextView)findViewById(R.id.tv0801));
        tv.add((TextView)findViewById(R.id.tv0802));
        tv.add((TextView)findViewById(R.id.tv0803));
        tv.add((TextView)findViewById(R.id.tv0804));
        tv.add((TextView)findViewById(R.id.tv0805));
        tv.add((TextView)findViewById(R.id.tv0806));

        for(int i = 0; i<6; ++i){
            if(i == 0) tv.get(i).setHeight(100);
            else tv.get(i).setHeight((height-100)/5);
            tv.get(i).setWidth((width-100)/7);
        }
        tv.get(0).setText("18:00-19:30");
        for(Lesson l : llist) {
            String s = l.getType() + " " + l.getName() + " " + l.getRoom();
            if(l.getDayNum() == 1) tv.get(1).setText(s);
            else if(l.getDayNum() == 2) tv.get(2).setText(s);
            else if(l.getDayNum() == 3) tv.get(3).setText(s);
            else if(l.getDayNum() == 4) tv.get(4).setText(s);
            else if(l.getDayNum() == 5) tv.get(5).setText(s);
        }

    }


}
