package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPreferences {
    protected static AppPreferences INSTANCE;
    private static SharedPreferences prefs;

    public static AppPreferences getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new AppPreferences();
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
        }

        return INSTANCE;
    }

    public void setPath(String path) {
        prefs.edit().putString("path", path).apply();
    }

    public String getPath() {
        return prefs.getString("path", "-1");
    }
}