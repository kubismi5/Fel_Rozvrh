package com.example.fel_rozvrh.fel_rozvrh;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Event{

    @Id
    private long id;
    private long idSub;

    private int hour;
    private int min;
    private int dayNum;
    private int month;

    private String name;
    private String time;
    private String day;
    private String text;
    private boolean itWas;

    public Event(String name,
                 String time,
                 String day,
                 String text){
        this.name = name;
        this.time = time;
        this.text = text;
        saveDay(day);
        saveTime(time);
        setItWas(true);
    }

    public Event( long id,
                  long idSub,
                  int hour,
                  int min,
                  int dayNum,
                  int month,
                  String name,
                  String time,
                  String day,
                  String text,
                  boolean itWas) {
        this.id = id;
        this.idSub = idSub;
        this.hour = hour;
        this.min = min;
        this.dayNum = dayNum;
        this.month = month;
        this.name = name;
        this.time = time;
        this.day = day;
        this.text= text;
        this.itWas = itWas;
    }


    private void saveTime(String time) {
        min = hour = 0;
        boolean ok = true;
        int i = 0;
        if (time.length() > 2 && time.length() < 6) {
            hour = time.charAt(i++) - 48;
            if (time.charAt(i) != ':') hour = (hour * 10) + time.charAt(i++) - 48;
            if (time.charAt(i++) != ':') {
                i = 0;
                ok = false;
            }
            min = time.charAt(i++) - 48;
            if (time.length() == i + 1) min = (min * 10) + time.charAt(i) - 48;
            if (min > 59 || hour > 23) ok = false;
        }
        if (!ok) {
            min = hour = 0;
            this.time = "error";
        } else {
            this.time = time;
        }
    }
    private boolean isNumber(char a){
        int n = a - 48;
        return -1 < n && n < 10;
    }
    private void saveDay(String d){
        dayNum = month = 0;
        boolean ok = true;
        int i = 0;
        if (d.length() > 2 && d.length() < 6) {
            dayNum = d.charAt(i++) - 48;
            if (isNumber(d.charAt(i))) dayNum = (dayNum * 10) + d.charAt(i++) - 48;
            if (isNumber(d.charAt(i++))) {
                i = 0;
                ok = false;
            }
            month = d.charAt(i++) - 48;
            if (d.length() == i + 1) month = (month* 10) + d.charAt(i) - 48;
            if (dayNum > 31 || month > 12) ok = false;
        }
        if (!ok) {
            dayNum = month = 0;
            this.day= "error";
        } else {
            this.day = d;
        }
    }

    public long getId() {
        return id;
    }

    public long getIdSub() {
        return idSub;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public int getDayNum() {
        return dayNum;
    }

    public String getDay() {
        return day;
    }

    public String getTime() {
        return time;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setIdSub(long idSub) {
        this.idSub = idSub;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setDayNum(int dayNum) {
        this.dayNum = dayNum;
    }

    public void setDay(String day) {
        saveDay(day);
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMonth() {
        return month;
    }

    public String getText() {
        return text;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isItWas() {
        return itWas;
    }

    public void setItWas(boolean itWas) {
        this.itWas = itWas;
    }
}
