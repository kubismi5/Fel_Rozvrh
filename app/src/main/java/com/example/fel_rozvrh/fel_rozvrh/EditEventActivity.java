package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.LinkedList;
import java.util.List;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class EditEventActivity extends AppCompatActivity implements View.OnClickListener{


    Context context;

    private Event event;
    private LinkedList<Event> eveList = new LinkedList<>();
    private Box<Event> eveBox;
    private Query<Event> eveQuery;
    private EditText eName;
    private EditText eTime;
    private EditText eDay;
    private EditText eText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);

        Intent i = getIntent();
        long id = i.getLongExtra("a", 1);

        eveList = new LinkedList<>();

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        eveBox = boxStore.boxFor(Event.class);

        eveQuery = eveBox.query().build();
        List<Event> eveQueryResult = eveQuery.find();
        eveList.clear();
        eveList.addAll(eveQueryResult);

        for(Event s : eveList){
            if(s.getId() == id){
                event = s;
                break;
            }
        }
        //View mView = getLayoutInflater().inflate(R.layout.edit_link, null);
        eName= (EditText) findViewById(R.id.edit_event_name);
        eTime= (EditText) findViewById(R.id.edit_event_time);
        eDay= (EditText) findViewById(R.id.edit_event_day);
        eText= (EditText) findViewById(R.id.edit_event_text);
        eName.setText(event.getName());
        eTime.setText(event.getTime());
        eDay.setText(event.getDay());
        eText.setText(event.getText());
        Button buttonSave = (Button) findViewById(R.id.btnSaveEditEvent);
        Button buttonRemove = (Button) findViewById(R.id.btnRemoveEditEvent);

        buttonSave.setOnClickListener(this);
        buttonRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnSaveEditEvent){
            if(eName.getText().toString().equals(""))
                event.setName("error");
            else event.setName(eName.getText().toString());

            if(eTime.getText().toString().equals(""))
                event.setTime("error");
            else event.setTime(eTime.getText().toString());

            if(eDay.getText().toString().equals(""))
                event.setDay("error");
            else event.setDay(eDay.getText().toString());

            event.setText(eText.getText().toString());

            eveBox.put(event);
            finish();
        } else if(v.getId() == R.id.btnRemoveEditEvent) {
            List<Event> eveQueryResult = eveQuery.find();
            eveList.clear();
            eveList.addAll(eveQueryResult);
            LinkedList<Event> newList = new LinkedList<>();
            for (Event e : eveList) {
                if (e.getId() != event.getId()) {
                    newList.add(e);
                }
            }

            eveQuery.remove();
            eveList.clear();
            eveList.addAll(eveQuery.find());

            for (Event e : newList) {
                eveBox.put(e);
            }
            finish();
        }
    }
}


