package com.example.fel_rozvrh.fel_rozvrh;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public class AddSubjectDialog extends DialogFragment {
    private Box<Subject> subBox;

    private TextView mTextViewName;
    private TextView mTextViewCode;

    public interface NoticeDialogListener {
        public void onDialogPositiveClick(Subject subject);
        public void onDialogNegativeClick();
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BoxStore boxStore = ((TATApplication) getActivity().getApplication()).getBoxStore();
        subBox = boxStore.boxFor(Subject.class);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
//        System.out.println(subBox.count());
//        System.out.println("12345678");
        View view = inflater.inflate(R.layout.dialog_add_subject, null);
        mTextViewName = (TextView) view.findViewById(R.id.dialog_add_sub_name);
        mTextViewCode = (TextView) view.findViewById(R.id.dialog_add_sub_code);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.button_save_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Subject sub = new Subject(subBox.count(), mTextViewName.getText().toString(),mTextViewCode.getText().toString());
                        mListener.onDialogPositiveClick(sub);
                    }
                })
                .setNegativeButton(R.string.button_cancel_label, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddSubjectDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        //mTextView = (TextView) getDialog().findViewById(R.id.dialog_add_poi_name_text);
    }
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.dialog_add_poi, container, false);
//        mTextView = (TextView) view.findViewById(R.id.dialog_add_poi_name_text);
//        return view;
//    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }
}
