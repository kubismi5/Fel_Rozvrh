package com.example.fel_rozvrh.fel_rozvrh;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.InputType;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class DynamicViews {
    private Context ctx;

    DynamicViews(Context ctx){
        this.ctx = ctx;
    }

    @SuppressLint("SetTextI18n")
    public TextView descriptionTextView(Context context, String text){
        final ViewGroup.LayoutParams lparams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final TextView textView = new TextView(context);
        textView.setLayoutParams(lparams);
        textView.setTextSize(20);
        textView.setTextColor(Color.rgb(99,0,0));
        textView.setText(" "+text+" ");
        textView.setMaxEms(8);
        return textView;
    }

    public EditText receivedQuantityEditText(Context context){
        final ViewGroup.LayoutParams lparams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText editText = new EditText(context);
        int id = 0;
        editText.setId(id);
        editText.setMinEms(2);
        //editText.setTextColor(Color.rgb(1,1,1));
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        return editText;
    }

    public TextView priceogItem(Context context, String text){
        final ViewGroup.LayoutParams lparams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final TextView textView = new TextView(context);
        textView.setLayoutParams(lparams);
//        textView.setTextSize(10);
//        textView.setTextColor(Color.rgb(0,0,0));
        textView.setText(text);
        return textView;
    }


}

