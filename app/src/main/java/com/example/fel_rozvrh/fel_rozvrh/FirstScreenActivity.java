package com.example.fel_rozvrh.fel_rozvrh;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridLayout;
import android.support.v7.widget.CardView;
import android.widget.RemoteViews;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class FirstScreenActivity extends AppCompatActivity implements View.OnClickListener {

    private LinkedList<Subject> subList = new LinkedList<>();
    private Box<Subject> subBox;
    private Query<Subject> subQuery;
    private MyLinkListAdapter mAdapterLink;
    private RecyclerView mRecyclerViewLink;
    private Button settings;

    DynamicViews dnv;
    Context context;
    LinkedList<TextView> subTextView = new LinkedList<>();

    private GridLayout sublayout;
    private int lastSize;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_first_screen);
        setSubTextView();
        sublayout = (GridLayout) findViewById(R.id.subject_gridlayout);

        Button subButton = (Button)findViewById(R.id.goToSubjectsButton);
        Button timetableButton = (Button)findViewById(R.id.buttonTimetable);
        Button eventButton = (Button)findViewById(R.id.buttonEvents);
        settings = (Button)findViewById(R.id.buttonSettings);

        subButton.setOnClickListener(this);
        timetableButton.setOnClickListener(this);
        eventButton.setOnClickListener( this);
        settings.setOnClickListener(this);
        setToggleEvent(true);

        newNotificatioin();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.goToSubjectsButton){
            Context context = v.getContext();
            Intent intent = new Intent(context, SubjectListActivity.class);
            context.startActivity(intent);
        } else if(v.getId() == R.id.buttonTimetable){
            Context context = v.getContext();
            Intent intent = new Intent(context, TimetableActivity.class);
            context.startActivity(intent);
        }
        else if(v.getId() == R.id.buttonEvents){
            Context context = v.getContext();
            Intent intent = new Intent(context, EventListActivity.class);
            context.startActivity(intent);
        }
        else if(v.getId() == R.id.buttonSettings){
            Context context = v.getContext();
            Intent intent = new Intent(context, SettingsActivity.class);
            startActivity(intent);
        }

    }
    @Override
    public void onResume(){
        super.onResume();
        setToggleEvent(false);
    }

    private void newNotificatioin(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String date = simpleDateFormat.format(calendar.getTime());
        int day = ((date.charAt(0)-48)*10) + date.charAt(1) - 48;
        int month = ((date.charAt(3)-48)*10) + date.charAt(4) - 48;

        NotificationCompat.Builder builder;
        NotificationManager notificationManager;
        int notification_id;
        RemoteViews remoteViews;
        Context context02 = this;
        LinkedList<Event> eveList = new LinkedList<>();
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        Box<Event> eveBox = boxStore.boxFor(Event.class);
        Query<Event> eveQuery = eveBox.query().build();
        List<Event> eveQueryResult = eveQuery.find();
        eveList.addAll(eveQueryResult);

        if(!eveList.isEmpty()) {
            LinkedList<Event> newEveList = new LinkedList<>();
            for (Event e : eveList)
                if (e.getMonth() == month && e.getDayNum() == day)
                    newEveList.add(e);
            for(Event e : newEveList) {
                if (e.isItWas()) {
                    notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    builder = new NotificationCompat.Builder(this);

                    remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);
                    remoteViews.setTextViewText(R.id.notif_event_name, e.getName());
                    remoteViews.setTextViewText(R.id.notif_event_time, e.getTime());
                    remoteViews.setTextViewText(R.id.notif_event_day, e.getDay());

                    notification_id = (int) System.currentTimeMillis();

                    Intent notification_intent = new Intent(context02, FirstScreenActivity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context02, 0, notification_intent, 0);

                    builder.setSmallIcon(R.mipmap.ic_launcher)
                            .setAutoCancel(true)
                            .setCustomBigContentView(remoteViews)
                            .setContentIntent(pendingIntent);

                    notificationManager.notify(notification_id, builder.build());
                    e.setItWas(false);
                }
            }
        }
    }


    private void restart(){
        finish();
        startActivity(getIntent());
    }

    private void setToggleEvent(boolean firstime){
        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        subBox = boxStore.boxFor(Subject.class);

        subQuery = subBox.query().build();
        List<Subject> subQueryResult = subQuery.find();
        subList.clear();
        subList.addAll(subQueryResult);

        if(firstime)lastSize = subList.size();
        else {
            if(lastSize!= subList.size()){
                restart();
                return;
            }
        }
        sublayout = (GridLayout) findViewById(R.id.subject_gridlayout);

        for (int i = 0; i < sublayout.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) sublayout.getChildAt(i);
            if(i<subList.size()) {
                final int finalI = i;
                subTextView.get(i).setText(subList.get(i).getName());
                cardView.setMinimumHeight(50);

                cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Context context = view.getContext();
                        Intent intent = new Intent(context, SubjectActivity.class);
                        intent.putExtra("a", subList.get(finalI).getId());
                        context.startActivity(intent);
                    }
                });
            } else {
                cardView.removeAllViews();
            }
        }
    }

    private void setSubTextView(){
        subTextView.add((TextView)findViewById(R.id.sub1));
        subTextView.add((TextView)findViewById(R.id.sub2));
        subTextView.add((TextView)findViewById(R.id.sub3));
        subTextView.add((TextView)findViewById(R.id.sub4));
        subTextView.add((TextView)findViewById(R.id.sub5));
        subTextView.add((TextView)findViewById(R.id.sub6));
        subTextView.add((TextView)findViewById(R.id.sub7));
        subTextView.add((TextView)findViewById(R.id.sub8));
        subTextView.add((TextView)findViewById(R.id.sub9));
        subTextView.add((TextView)findViewById(R.id.sub10));
        subTextView.add((TextView)findViewById(R.id.sub11));
        subTextView.add((TextView)findViewById(R.id.sub12));
        subTextView.add((TextView)findViewById(R.id.sub13));
        subTextView.add((TextView)findViewById(R.id.sub14));
        subTextView.add((TextView)findViewById(R.id.sub15));
        subTextView.add((TextView)findViewById(R.id.sub16));
        subTextView.add((TextView)findViewById(R.id.sub17));
        subTextView.add((TextView)findViewById(R.id.sub18));
        subTextView.add((TextView)findViewById(R.id.sub19));
        subTextView.add((TextView)findViewById(R.id.sub20));
    }

}


