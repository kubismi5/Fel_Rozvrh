package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventViewHolder> {
    private final LinkedList<Event> mEventList;
    private LayoutInflater mInflater;

    EventListAdapter(Context context, LinkedList<Event> wordList) {
        mInflater = LayoutInflater.from(context);
        this.mEventList = wordList;
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.event_list_item, parent, false);
        return new EventViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        Event mCurrent = mEventList.get(position);
        holder.eveItemViewName.setText(mCurrent.getName());
        holder.eveItemViewTime.setText(mCurrent.getTime());
        holder.eveItemViewDay.setText(mCurrent.getDay());

    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }

    class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final TextView eveItemViewName;
        final TextView eveItemViewTime;
        final TextView eveItemViewDay;

        final EventListAdapter mAdapter;

        EventViewHolder(View itemView, EventListAdapter adapter) {
            super(itemView);
            eveItemViewName = (TextView) itemView.findViewById(R.id.event_name_label);
            eveItemViewTime = (TextView) itemView.findViewById(R.id.event_time_label);
            eveItemViewDay = (TextView) itemView.findViewById(R.id.event_day_label);

            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int mPosition = getLayoutPosition();
            long element = mEventList.get(mPosition).getId();
            Context context = view.getContext();
            Intent intent = new Intent(context, EditEventActivity.class);
            intent.putExtra("a", element);
            context.startActivity(intent);
        }
    }
}
