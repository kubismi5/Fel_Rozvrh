package com.example.fel_rozvrh.fel_rozvrh;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.LinkedList;
import java.util.List;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class EditSubjectActivity extends AppCompatActivity implements View.OnClickListener{


    Context context;

    private Subject sub;
    private LinkedList<Subject> subList = new LinkedList<>();
    private Box<Subject> subBox;
    private Query<Subject> subQuery;
    private EditText eCode;
    private EditText eName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_subject);

        Intent i = getIntent();
        long id = i.getLongExtra("a", 1);

        subList = new LinkedList<>();

        BoxStore boxStore = ((TATApplication) getApplication()).getBoxStore();
        subBox = boxStore.boxFor(Subject.class);

        subQuery = subBox.query().build();
        List<Subject> subQueryResult = subQuery.find();
        subList.clear();
        subList.addAll(subQueryResult);

        for(Subject s : subList){
            if(s.getId() == id){
                sub = s;
                break;
            }
        }
        //View mView = getLayoutInflater().inflate(R.layout.edit_link, null);
        eCode= (EditText) findViewById(R.id.dialog_add_edit_sub_code);
        eName= (EditText) findViewById(R.id.dialog_add_edit_sub_name);
        eCode.setText(sub.getCode());
        eName.setText(sub.getName());
        Button buttonSave = (Button) findViewById(R.id.btnSaveEditSub);
        Button buttonRemove = (Button) findViewById(R.id.btnRemoveEditSub);

        buttonSave.setOnClickListener(this);
        buttonRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnSaveEditSub){
            if(eName.getText().toString().equals(""))
                sub.setName("error");
            else
                sub.setName(eName.getText().toString());
            if(eCode.getText().toString().equals(""))
                sub.setCode("error");
            else
                sub.setCode(eCode.getText().toString());
            subBox.put(sub);
            finish();
        } else if(v.getId() == R.id.btnRemoveEditSub) {
            List<Subject> subQueryResult = subQuery.find();
            subList.clear();
            subList.addAll(subQueryResult);
            LinkedList<Subject> newList = new LinkedList<>();
            for (Subject s : subList) {
                if (s.getId() != sub.getId()) {
                    newList.add(s);
                }
            }

            subQuery.remove();
            subList.clear();
            subList.addAll(subQuery.find());

            for (Subject s : newList) {
                subBox.put(s);
            }
            finish();
        }
    }
}


